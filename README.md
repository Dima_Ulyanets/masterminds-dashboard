Errors format:
{
    "statusCode": 422,
    "error": "Unprocessable Entity",
    "message": "User creation failed",
    "data": {
        "email": "This email is already taken"
    }
}
data - optional


Please create .env file with following settings:

PORT=Number (ex.: 3000)
