const axios = require('axios');
const boom = require('boom');

const PARSER_API_BASE_URL = 'http://dev.mycroseo-parser.spacestep.ca:260';

const parserApiRequest = ({url, method = 'get', data, params}) => {
    return axios({
        url,
        method,
        params,
        baseURL: PARSER_API_BASE_URL,
        data
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            throw boom.internal('Parse API error', {status: error.response.status, data: error.response.data});
        })
};

module.exports = parserApiRequest;