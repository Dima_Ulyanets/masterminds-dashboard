const express = require('express');

const analyzeRoutes = require('./analyzeRoutes');
const snipsRoutes = require('./snipsRoutes');
const linksRoutes = require('./linksRoutes');
const settingsRoutes = require('./settingsRoutes');
const editorsRoutes = require('./editorsRoutes');

const router = express.Router();

router.use('/analyze', analyzeRoutes);
router.use('/snips', snipsRoutes);
router.use('/links', linksRoutes);
router.use('/settings', settingsRoutes);
router.use('/editors', editorsRoutes);

module.exports = router;
