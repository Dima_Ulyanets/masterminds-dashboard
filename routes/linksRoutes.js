const router = require('express-promise-router')();

const sheetController = require('../controllers/sheetConroller');

router.route('/')
    .get(sheetController.getClientLinks);
router.route('/')
    .post(sheetController.addLinks);

// https://stackoverflow.com/questions/21863326/delete-multiple-records-using-rest
router.route('/delete-bulk')
    .put(sheetController.deleteLinks);

router.route('/:id')
    .put(sheetController.updateLink);

module.exports = router;
