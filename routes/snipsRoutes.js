const router = require('express-promise-router')();

const snipsController = require('../controllers/snipsConroller');

router.route('/')
    .get(snipsController.getSnips);

router.route('/')
    .post(snipsController.createSnip);

router.route('/:snip_id')
    .delete(snipsController.deleteSnip);

router.route('/upload')
    .post(snipsController.uploadImage);

module.exports = router;
