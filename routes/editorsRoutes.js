const router = require('express-promise-router')();

const editorController = require('../controllers/editorController');

router.route('/')
    .get(editorController.getEditors);

router.route('/:id')
    .get(editorController.getEditor);

router.route('/')
    .post(editorController.createEditor);

module.exports = router;
