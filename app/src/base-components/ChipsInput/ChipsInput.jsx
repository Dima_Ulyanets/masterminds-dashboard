import React from 'react'
import { string } from 'prop-types'
import Typography from 'material-ui/Typography'
import Chips from 'react-chips'

const KEY_CODES = {
    ENTER: 13,
    COMA: 188
};

const ChipsInput = ({ meta: { valid, error }, input, placeholder}) => {

    const handleChipChange = chips => {
        input.onChange(chips);
    };

    return (
        <React.Fragment>
            {
                !valid ? <Typography noWrap color="error" align="left">{error}</Typography>: null
            }
            <Chips
                {...input}
                placeholder={placeholder}
                value = {input.value || []}
                onChange={handleChipChange}
                onBlur={() => input.onBlur()}
                createChipKeys={[KEY_CODES.ENTER, KEY_CODES.COMA]}
            />
        </React.Fragment>
    )
};

ChipsInput.propTypes = { placeholder: string };
ChipsInput.defaultProps = { placeholder: '' };

export default ChipsInput
