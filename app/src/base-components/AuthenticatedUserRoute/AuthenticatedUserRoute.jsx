import React from 'react';
import {Route, Redirect} from 'react-router-dom';

const AuthenticatedUserRoute = ({component: Component, componentProps, ...rest}) => {
    const route = props => {
        const {isLoggedIn} = componentProps;
        if (!isLoggedIn) {
            return <Redirect to='/signin' />
        }
        if (isLoggedIn) {
            return <Component {...props} {...componentProps}/>
        }
    };

    return <Route {...rest} render={route} />;
};


export default AuthenticatedUserRoute;
