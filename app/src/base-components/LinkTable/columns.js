import React from 'react';

import ActionsCell from './ActionsCell';
import CheckboxColumn from './CheckboxColumn';
import TextFieldColumnCell from './TextFieldColumnCell';
import SelectColumnCell from './SelectColumnCell';
import RangeFilter from './RangeFilter';
import SelectFilter from './SelectFilter';

import parseQuery from '../../helpers/parseQuery';

const getActionsColumn = (getActionProps) => ({
    Header: "",
    filterable: false,
    sortable: false,
    getProps: getActionProps,
    Cell: ActionsCell,
})

const getCheckboxColumn = ({ headerValue, onHeaderChange, cellValue, onCellChange, ...rest }) => ({
    Header: props => <CheckboxColumn.CheckboxColumnHeader checked={headerValue} onChange={onHeaderChange} />,
    accessor: '_id',
    sortable: false,
    filterable: false,
    Cell: props => <CheckboxColumn.CheckboxColumnCell checked={cellValue(props.value)} onChange={() => onCellChange(props.value)} />,
    ...rest,
})

const getStringColumn = ({
    name,
    accessor,
    editable = false,
    editing = false,
    value = '',
    onChange = () => {},
    ...rest,
}) => ({
    ...rest,
    Header: name,
    accessor,
    Cell: editable
        ? props => <TextFieldColumnCell type="text" editing={editing._id === props.original._id} value={value} onChange={onChange} cellProps={props}  />
        : props => <div>{props.value}</div>,
})

const getCurrencySimbol = currency => currency === 'USD' ? '$' : '€';

const getNumberColumn = ({
    name,
    id,
    showCurrency = false,
    min = 0,
    max = 20000,
    editable = false,
    editing = false,
    value = 0,
    onChange = () => {},
    ...rest,
}) => ({
    ...rest,
    Header: name,
    id: id,
    accessor: d => showCurrency ? `${getCurrencySimbol(d.currency)}${Number(d[id])}` : Number(d[id]),
    Filter: (props) => <RangeFilter defaultMin={min} defaultMax={max} {...props} />,
    filterMethod: (filter, row) => {
        let { min, max } = parseQuery(filter);
        const amount = !isNaN(row[id]) ? row[id] : parseFloat(row[id].substr(1))
        return amount >= min && (max ? amount <= max : true);
    },
    Cell: editable
        ? props => <TextFieldColumnCell type="number" editing={editing._id === props.original._id} value={value} onChange={onChange} cellProps={props}  />
        : props => <div>{props.value}</div>,
})

const getSelectColumn = ({
    name,
    accessor,
    options,
    getProps,
    editable = false,
    editing = false,
    value = 0,
    onChange = () => {},
    ...rest,
}) => ({
    ...rest,
    Header: name,
    accessor,
    Filter: ({ filter, onChange }) => <SelectFilter filter={filter} onChange={onChange} options={options} />,
    getProps,
    Cell: editable
        ? props => <SelectColumnCell editing={editing._id === props.original._id} value={value} onChange={onChange} options={options} cellProps={props} />
        : props => <div>{props.value}</div>,
})

export default {
    getActionsColumn,
    getCheckboxColumn,
    getStringColumn,
    getNumberColumn,
    getSelectColumn,
}