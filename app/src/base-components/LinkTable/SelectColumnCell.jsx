import React from 'react';

import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

const SelectColumnCell = ({ editing, value, onChange, options, cellProps }) => {
    return editing
        ? (
            <Select
                value={value}
                onChange={e => onChange(e.target.value)}
                style={{ width: "100%" }}
            >
                {options.map(opt => <MenuItem key={opt} value={opt}>{opt}</MenuItem>)}
            </Select>
        )
        : cellProps.value
};

export default SelectColumnCell;