import React from 'react';

import TextField from '@material-ui/core/TextField';

const TextFieldColumnCell = ({ editing, value, onChange, type, cellProps }) => {
    return editing
        ? <TextField
            type={type}
            value={value}
            onChange={e => onChange(e.target.value)}
            style={{ width: '100%' }}
        />
        : cellProps.value
};

export default TextFieldColumnCell;