import React, {Component} from 'react'
import {object, array, func} from 'prop-types'
import Drawer from 'material-ui/Drawer'
import List, {ListItem, ListItemText} from 'material-ui/List'
import Divider from 'material-ui/Divider'
import LogoImg from '../../assets/images/logo.png'

import * as styles from './Sidebar.module.scss'

export default class Sidebar extends Component {
    static propTypes = {
        classes: object.isRequired,
        items: array.isRequired,
        onItemClick: func.isRequired
    };

    render() {
        const {classes, items, onItemClick, ...restProps} =this.props;
        return (
            <div>
                <Drawer
                    variant="permanent"
                    open
                    classes={classes}
                    {...restProps}
                >
                    <div className={styles.sidebarLogo}>
                        <img src={LogoImg} alt="Smarter Microworkers"/>
                    </div>
                    <List>
                        <Divider/>
                        {items.map(item => (
                            <ListItem
                                button
                                divider
                                key={item.value}
                                onClick={() => onItemClick(item.value)}>
                                <ListItemText primary={item.label} />
                            </ListItem>
                        ))}
                    </List>
                </Drawer>
            </div>
        );
    }
}

