export default {
    production: {
        PARSER_DOMAIN: 'http://scraper-prod-env.bfvdxpdzmf.us-west-1.elasticbeanstalk.com',
        DOMAIN: 'http://linkplate.com',
        API_DOMAIN: 'http://linkplate.com/api',
    },
    staging: {
        PARSER_DOMAIN: 'http://scraper-prod-env.bfvdxpdzmf.us-west-1.elasticbeanstalk.com',
        DOMAIN: 'http://linkplate.com',
        API_DOMAIN: 'http://linkplate.com/api',
    },
    development: {
        PARSER_DOMAIN: 'http://dev.mycroseo-parser.spacestep.ca:260',
        DOMAIN: 'http://linkplate.com',
        API_DOMAIN: 'http://linkplate.com/api',
    },
    local: {
        PARSER_DOMAIN: 'http://dev.mycroseo-parser.spacestep.ca:260',
        DOMAIN: 'http://localhost:3000',
        API_DOMAIN: 'http://localhost:3001/api',
    },
}
