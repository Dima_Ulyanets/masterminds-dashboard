import { getToken } from './auth';

export const getDefaultHeaders = () => {

    let headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + getToken()
    };

    return headers;
};

export default function(route, method = "GET", data, headers = getDefaultHeaders(), stringifyData = true) {
    data = stringifyData ? JSON.stringify(data) : data;

    return fetch(route, {
        method: method,
        headers: headers,
        body: data
    })
        .then(res => {
            return res;
        })
};
