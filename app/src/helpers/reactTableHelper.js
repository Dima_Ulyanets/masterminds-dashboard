export function defaultFilter(filter, row) {
  if (row[filter.id]) {
      return String(row[filter.id].toLocaleLowerCase()).indexOf(filter.value.toLocaleLowerCase()) > -1
  }
}

export function highlightRow (state, rowInfo, column, instance) {
  if (rowInfo && rowInfo.row && rowInfo.row.link) {
    const link = rowInfo.row.link.toLowerCase();
    if (link === 'nofollow') {
      return {
        style: {
          textDecoration: 'line-through'
        }
      }
    }
  }
  return {}
}