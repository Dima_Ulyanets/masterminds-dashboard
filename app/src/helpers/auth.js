
export const setToken = token => {
    localStorage.setItem('seo_token', token);
};

export const getToken = () => {
    return localStorage.getItem('seo_token') || sessionStorage.getItem('seo_token');
};

export const hasToken = () => {
    return Boolean(getToken());
};

export const removeToken = () => {
    sessionStorage.removeItem('seo_token');
    localStorage.removeItem('seo_token');
};

export const signOut = () => {
    removeToken();
    window.location.reload();
};
