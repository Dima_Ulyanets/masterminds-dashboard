import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'
import Routes from './Routes'

import * as styles from './App.module.scss';


class App extends Component {

    render() {
        return (
            <div className={styles.routeContainer}>
                <Routes/>
            </div>
        )
    }
}

export default withRouter(App);
