import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { SnackbarProvider } from 'notistack';

import store from './store';

//global styles
import 'normalize.css/normalize.css';
import './index.scss';

ReactDOM.render(
    <Provider store={store}>
        <SnackbarProvider
            maxSnack={3}
            anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }}
        >
            <Router>
                <App />
            </Router>
        </SnackbarProvider>
    </Provider>,
    document.getElementById('root'));
registerServiceWorker();
