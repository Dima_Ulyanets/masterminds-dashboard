import { call, put, takeEvery } from 'redux-saga/effects';
import config from '../../config';
import * as constants from './constants';
import * as actions from './actions';
import req from '../../helpers/request';

export function* getSnips() {
    const url = config[process.env.REACT_APP_ENV].API_DOMAIN + '/snips';
    const response = yield call(req, url, 'GET');
    const body = yield response.json();

    if (response.ok) {
        yield put(actions.getSnipsSuccess(body));
    } else {
        throw response;
    }
}
export function* createSnip({ snip }) {
    const url = config[process.env.REACT_APP_ENV].API_DOMAIN + '/snips';
    let imageUrl = null;
    if (snip.image) {
        const data = new FormData();
        data.append('file', snip.image);
        const uploadResponse = yield call(req, `${url}/upload`, 'POST', data, {}, false);
        const uploadBody = yield uploadResponse.json();
        imageUrl = uploadBody.imageUrl;
    }

    const response = yield call(req, url, 'POST', {...snip, imageUrl});
    const body = yield response.json();

    if (response.ok) {
        yield put(actions.createSnipSuccess(body));
    } else {
        throw response;
    }
}

export function* deleteSnip({ snipId }) {
    const url = config[process.env.REACT_APP_ENV].API_DOMAIN + `/snips/${snipId}`;
    const response = yield call(req, url, 'DELETE');

    if (response.ok) {
        yield put(actions.deleteSnipSuccess(snipId));
    } else {
        throw response;
    }
}

export default function* () {
    yield takeEvery(constants.GET_SNIPS_REQUEST, getSnips);
    yield takeEvery(constants.CREATE_SNIP_REQUEST, createSnip);
    yield takeEvery(constants.DELETE_SNIP_REQUEST, deleteSnip);
}
