import React from 'react'
import { Button, Card, CardContent, CardMedia, Typography } from '@material-ui/core';
import logoImage from '../../../assets/images/image.jpg'
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    card: {
        display: 'flex',
        marginTop: 25,
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
    },
    cover: {
        width: 151,
    },
    bar: {
        position: 'fixed',
        bottom: 30,
        left: 30,
    },
    btnWrapper: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginTop: 10,
    }
});

const SnipCard = ({ classes, snip }) => {
    const { message, buttonText, buttonUrl, imageUrl } = snip;
    const createSnip = (websiteUrl, message, buttonText, buttonUrl) => {
        window.open(`snip?websiteUrl=${websiteUrl}&message=${message}&buttonText=${buttonText}&buttonUrl=${buttonUrl}&imageUrl=${imageUrl}`, "_blank")
    };

    return (
        <Card className={classes.card}>
            <CardMedia
                className={classes.cover}
                image={snip.imageUrl || logoImage}
                title="Zaid"
            />
            <div className={classes.details}>
                <CardContent className={classes.content}>
                    <Typography component="p" variant="subtitle1">
                        {message}
                    </Typography>
                    <div className={classes.btnWrapper}>
                        <Button
                            variant="contained"
                            color="primary"
                            size="small"
                            onClick={() => createSnip(buttonUrl, message, buttonText, buttonUrl, imageUrl)}
                        >
                            {buttonText}
                        </Button>
                    </div>
                </CardContent>
            </div>
        </Card>
    )
};


export default (withStyles(styles, { withTheme: true })(SnipCard));
