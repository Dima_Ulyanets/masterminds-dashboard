import React from 'react'
import { Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import SnipCard from './SnipCard'

const styles = theme => ({
    snipList: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    deleteBtn: {
        position: 'relative',
        top: 60,
        left: 10,
    }
});

const SnipList = ({ classes, snips, handleDelete }) => {
    const createSnip = (websiteUrl, message, buttonText, buttonUrl, imageUrl) => {
        window.open(`snip?websiteUrl=${websiteUrl}&message=${message}&buttonText=${buttonText}&buttonUrl=${buttonUrl}&imageUrl=${imageUrl}`, "_blank")
    };
    return (
        <div className={classes.snipList}>
            {snips.map(snip => {
                return <div>
                    <Button
                        className={classes.deleteBtn}
                        variant="contained"
                        color="secondary"
                        size="small"
                        onClick={() => handleDelete(snip._id)}
                    > Delete </Button>
                    <Button
                        className={classes.deleteBtn}
                        variant="contained"
                        color="primary"
                        size="small"
                        onClick={() => createSnip(snip.websiteUrl, snip.message, snip.buttonText, snip.buttonUrl, snip.imageUrl)}
                    > View </Button>
                    <SnipCard snip={snip} />
                </div>
            })}
        </div>
    )
};


export default (withStyles(styles, { withTheme: true })(SnipList));
