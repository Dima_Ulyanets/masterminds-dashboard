import * as constants from './constants';
const initialState = {
    list: [],
};

export default function (state = initialState, action) {
    switch (action.type) {
        case constants.GET_SNIPS_REQUEST: {
            return {
                ...state,
                list: [],
                isFetching: true,
            };
        }

        case constants.GET_SNIPS_SUCCESS: {
            return {
                ...state,
                list: action.snips.slice(),
                isFetching: false,
            };
        }

        case constants.CREATE_SNIP_SUCCESS: {
            let newSnips = state.list.slice();
            newSnips.unshift({...action.snip});

            return {
                ...state,
                list: newSnips,
            };
        }

        case constants.DELETE_SNIP_SUCCESS: {
            let newSnips = state.list.slice();
            const snipIndex = newSnips.findIndex((snip) => snip._id === action.snipId);
            delete newSnips[snipIndex];
            return {
                ...state,
                list: newSnips,
            };
        }

        default:
            return state;
    }
}
