import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import queryString from 'query-string';
import { withStyles } from '@material-ui/core/styles';
import SnipCard from '../../components/SnipCard';

const styles = theme => ({
    card: {
        display: 'flex',
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
    },
    cover: {
        width: 151,
    },
    bar: {
        position: 'fixed',
        bottom: 30,
        left: 30,
    },
    btnWrapper: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginTop: 10,
    },
    iframe: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        border: 0,
        height: '100%',
        minWidth: '100%',
        width: 10,
        overflowX: 'auto'
    }
});

class SnipPage extends Component {

    render() {
        const { classes } = this.props;
        const snip = queryString.parse(this.props.location.search);
        return (
            <Fragment>
                <iframe src={snip.websiteUrl} className={classes.iframe} title="snip-frame" />
                <div className={classes.bar}>
                    <SnipCard snip={snip} />
                </div>
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        isFetching: state.snips.isFetching,
    }
};

const mapDispatchToProps = dispatch => {
    return {
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, { withTheme: true })(SnipPage));
