import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Button, Card, TextField } from '@material-ui/core';
import { getSnipsRequest, createSnipRequest, deleteSnipRequest } from '../../actions'
import SnipList from '../../components/SnipList'

class SnipCreator extends Component {

    state = {
        websiteUrl: 'https://www.space.com/',
        message: 'Hey, what\'s up? Visit my page to learn more...',
        buttonText: 'Click here',
        buttonUrl: 'https://ppcmasterminds.com'
    };

    componentDidMount() {
        this.props.getSnips();
    }

    handleFormChange = (key, value) => {
        const state = { ...this.state };
        state[key] = value;
        this.setState(state);
    };

    createSnip = () => {
        const { websiteUrl, message, buttonText, buttonUrl, image } = this.state;
        this.props.createSnip({ websiteUrl, message, buttonText, buttonUrl, image });
        // window.open(`snip?websiteUrl=${websiteUrl}&message=${message}&buttonText=${buttonText}&buttonUrl=${buttonUrl}`, "_blank")
    };

    render() {
        const { snips } = this.props;
        const { websiteUrl, message, buttonText, buttonUrl } = this.state;
        return (
            <div style={{ margin: '0 auto', width: 900 }}>
                <Card style={{ marginTop: 30 }}>
                    <form style={{ margin: '0 auto', width: 400, marginTop: 50, marginBottom: 50 }} onSubmit={(e) => {
                        e.preventDefault();
                        this.createSnip();
                    }}>
                        <TextField
                            style={{
                                width: 200,
                            }}
                            variant="outlined"
                            type='text'
                            margin="normal"
                            label={'Enter a URL'}
                            value={websiteUrl}
                            onChange={e => this.handleFormChange('websiteUrl', e.target.value)}
                        />
                        <TextField
                            style={{
                                width: 200,
                            }}
                            type='text'
                            variant="outlined"
                            margin="normal"
                            label={'Enter a message'}
                            value={message}
                            onChange={e => this.handleFormChange('message', e.target.value)}
                        />
                        <TextField
                            style={{
                                width: 200,
                            }}
                            type='text'
                            variant="outlined"
                            margin="normal"
                            label={'Enter a button text'}
                            value={buttonText}
                            onChange={e => this.handleFormChange('buttonText', e.target.value)}
                        />
                        <TextField
                            style={{
                                width: 200,
                            }}
                            type='text'
                            variant="outlined"
                            margin="normal"
                            label={'Enter a button URL'}
                            value={buttonUrl}
                            onChange={e => this.handleFormChange('buttonUrl', e.target.value)}
                        />
                        <input
                            accept="image/*"
                            style={{ display: 'none' }}
                            id="contained-button-file"
                            onChange={e => this.handleFormChange('image', e.target.files[0])}
                            type="file"
                        />
                        <label htmlFor="contained-button-file">
                            <Button variant="contained" component="span">
                                Upload Image
                        </Button>
                        </label>
                        <div style={{ textAlign: 'center' }}>
                            <Button
                                variant='contained'
                                color='primary'
                                type='submit'> Submit </Button>
                        </div>
                    </form>
                </Card>

                <SnipList snips={snips} handleDelete={this.props.deleteSnip} />
            </div>)
    }
}

const mapStateToProps = state => {
    return {
        snips: state.snips.list,
        isFetching: state.snips.isFetching,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getSnips: () => dispatch(getSnipsRequest()),
        createSnip: snip => dispatch(createSnipRequest(snip)),
        deleteSnip: snipId => dispatch(deleteSnipRequest(snipId))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SnipCreator);
