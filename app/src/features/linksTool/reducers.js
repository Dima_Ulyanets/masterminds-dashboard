import * as constants from './constants';
import config from '../../config';

const initialState = {
    serp: {},
    links: [],
    editors: [],
    margin: 1,
    fee: 1,
    isFetching: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case constants.SCRAPE_GOOGLE_REQUEST:

            return {
                ...state,
                serp: [],
                isFetching: true,
            };
        case constants.SCRAPE_GOOGLE_SUCCESS:

            return {
                ...state,
                serp: Object.values(action.serp.res.results[0].links)
            };

        case constants.SCRAPE_URL_SUCCESS:

            return {
                ...state,
                analysis: action.data,
                isFetching: false,
            };

        case constants.GET_CLIENT_LINKS_REQUEST:

            return {
                ...state,
                isFetching: true
            };

        case constants.GET_CLIENT_LINKS_SUCCESS:

            return {
                ...state,
                links: action.data.links,
                margin: action.data.margin,
                fee: action.data.fee,
                isFetching: false
            };

        case constants.CREATE_SETTINGS_REQUEST: {
            return {
                ...state,
                isFetching: true,
            };
        }

        case constants.CREATE_SETTINGS_SUCCESS: {
            return {
                ...state,
                isFetching: false,
                margin: action.margin || state.margin,
                fee: action.fee || state.fee
            };
        }

        case constants.GET_SETTINGS_REQUEST: {
            return {
                ...state,
                isFetching: true,
            };
        }

        case constants.GET_SETTINGS_SUCCESS: {
            return {
                ...state,
                isFetching: false,
                margin: action.margin,
                fee: action.fee
            };
        }

        case constants.DELETE_LINKS_REQUEST: {
            return {
                ...state,
                isFetching: true
            };
        }

        case constants.DELETE_LINKS_SUCCESS: {
            return {
                ...state,
                isFetching: false,
                links: removeDeletedLinks(state.links, action.deletedIds)
            };
        }

        case constants.ADD_EDITOR_REQUEST: {
            return {
                ...state,
                isFetching: true,
            };
        }

        case constants.ADD_EDITOR_SUCCESS: {
            return {
                ...state,
                isFetching: false,
                editors: [...state.editors, action.editor]
            };
        }

        case constants.GET_EDITORS_REQUEST: {
            return {
                ...state,
                isFetching: true,
            };
        }

        case constants.GET_EDITORS_SUCCESS: {
            return {
                ...state,
                isFetching: false,
                editors: [...action.editors]
            };
        }

        case constants.UPDATE_LINK_REQUEST: {
            return {
                ...state,
                isFetching: true,
            };
        }

        case constants.UPDATE_LINK_SUCCESS: {
            return {
                ...state,
                isFetching: false,
                links: state.links.map(item => {
                    if(item._id !== action.link._id) {
                        return item;
                    }
                    return {
                        ...item,
                        ...action.link,
                    }
                })
            };
        }

        default:
            return state;
    }
}

export const getLinksWithCurrentSettings = (state) => {
    const { margin, fee } = state.analyzer;
    return state.analyzer.links.map(link => ({
        ...link,
        price: updatePriceWithMargin(link.price, margin),
        convertedPrice: updatePriceWithMargin(link.convertedPrice, margin),
        profit: calculateProfit(link.convertedPrice, margin),
        net: calculateNetProfit(link.convertedPrice, margin, fee)
    }))
};

export const getEditorsWithUrl = state => {
    const { editors } = state.analyzer;
    return editors.map(editor => ({name: editor.name, url: generateUrlFromId(editor._id)}))
}

const generateUrlFromId = id => {
    return `${config[process.env.REACT_APP_ENV].DOMAIN}/import-editor/${id}`
}

const updatePriceWithMargin = (price, margin) => rounding(price * margin)

const calculateProfit = (price, margin) => rounding((price * margin) - price)

const calculateNetProfit = (price, margin, fee) => {
    const profit = rounding((price * margin) - price)
    return rounding(profit - (profit * fee / 100));
}

const rounding = number => parseFloat(number).toFixed(1)

const removeDeletedLinks = (currentLinks, deletedIds) => currentLinks.filter(link => !deletedIds.includes(link._id))
