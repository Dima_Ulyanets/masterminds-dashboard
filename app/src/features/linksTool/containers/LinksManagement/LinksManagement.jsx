import React, { Component } from 'react'

import ReactTable from 'react-table'
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import cx from 'classnames';
import isEmpty from 'lodash/isEmpty';

import queryString from 'stringquery'
import { connect } from 'react-redux'
import { getClientLinksRequest, getSettingsRequest, deleteLinksRequest } from '../../actions'
import { getLinksWithCurrentSettings } from '../../reducers'
import Loading from '../../../../base-components/Loading/Loading'
import { defaultFilter } from '../../../../helpers/reactTableHelper';
import columnDefs from '../../../../base-components/LinkTable/columns';
import styles from '../../../../assets/jss/react-table'

function updateSelectedStatus(array, isSelected) {
  return array.reduce((obj, item) => {
    obj[item._id] = isSelected;
    return obj
  }, {})
}

class LinksManagement extends Component {
  state = {
    seletedLinkIds: {},
    selectAll: false
  }

  componentDidMount() {
    this.props.getClientLinks();
  }

  static getDerivedStateFromProps(props, state) {
    if (props.links.length > 0 && isEmpty(state.seletedLinkIds)) {
      return {
        seletedLinkIds: updateSelectedStatus(props.links, state.selectAll),
      };
    }
    return null;
  }

  handleFilterChange = (filters) => {
    const searchParams = new URLSearchParams();
    filters.map(filter => searchParams.set(filter.id, filter.value));

    this.props.history.push(`?${searchParams}`);
  };

  selectAll = () => {
    const { selectAll } = this.state;
    this.setState({ selectAll: !selectAll }, () => {
      const seletedLinkIds = updateSelectedStatus(this.props.links, this.state.selectAll)
      this.setState({ seletedLinkIds })
    })
  }

  checkAndUpdateSelectAll = () => {
    const { seletedLinkIds } = this.state;
    const keys = Object.keys(seletedLinkIds);
    const filtered = keys.map(key => seletedLinkIds[key]).filter(selected => selected);
    if (filtered.length === 0) {
      this.setState({ selectAll: false });
    } else if (filtered.length === keys.length) {
      this.setState({ selectAll: true })
    }
  }

  handleSelected = (id) => {
    const selectedLinks = { ...this.state.seletedLinkIds };
    if (selectedLinks.hasOwnProperty(id)) {
      selectedLinks[id] = !selectedLinks[id];
    } else {
      selectedLinks[id] = true;
    }
    this.setState({ seletedLinkIds: selectedLinks }, this.checkAndUpdateSelectAll);
  }

  handleDeleteLinks = () => {
    const { seletedLinkIds } = this.state;
    const selectedLinks = Object.keys(seletedLinkIds)
      .map(key => ({ id: key, selected: seletedLinkIds[key] }))
      .filter(link => link.selected)
      .map(link => link.id);
    this.props.deleteLinks(selectedLinks);
  }

  render() {
    const { links, isFetching, classes } = this.props;
    const columns = [
      columnDefs.getCheckboxColumn({
        headerValue: this.state.selectAll,
        onHeaderChange: this.selectAll,
        cellValue: value => this.state.seletedLinkIds[value],
        onCellChange: value => this.handleSelected(value),
        width: 80,
      }),
      columnDefs.getStringColumn({
        name: 'Editor Name',
        accessor: 'editor.name',
      }),
      columnDefs.getStringColumn({
        name: 'Domain URL',
        accessor: 'url',
        width: 300,
      }),
      columnDefs.getNumberColumn({
        name: 'TAT',
        id: 'tat',
        min: 0,
        max: 120,
      }),
      columnDefs.getSelectColumn({
        name: 'Link Type',
        accessor: 'link',
        options: ['Dofollow', 'Nofollow'],
        getProps: (state, rowInfo, column) => {
          return {
            style: {
              textDecoration: rowInfo && rowInfo.row && rowInfo.row.link && rowInfo.row.link.toLowerCase().trim() === 'nofollow' ? 'line-through' : 'none',
            },
          };
        },
      }),
      columnDefs.getStringColumn({
        name: 'Anchor Type',
        accessor: 'anchor',
      }),
      columnDefs.getNumberColumn({
        name: 'Price ($)',
        id: 'price',
        min: 0,
        max: 10000,
        showCurrency: true,
      }),
      columnDefs.getNumberColumn({
        name: 'Profit ($)',
        id: 'profit',
      }),
      columnDefs.getNumberColumn({
        name: 'Net Profit ($)',
        id: 'net',
      }),
      columnDefs.getStringColumn({
        name: 'Notes',
        accessor: 'notes',
      }),
    ];
    const values = Object.values(queryString(this.props.location.search));
    const keys = Object.keys(queryString(this.props.location.search));

    const filters = keys.map((key, index) => {
      return {
        id: key,
        value: decodeURIComponent(values[index])
      };
    });

    return (
      <div className={classes.container}>
        <Card className={classes.card}>
          <CardHeader
            className={classes.cardHeader}
            action={
              <Button variant="contained" color="secondary" onClick={this.handleDeleteLinks}>
                Delete selected links
              </Button>
            }
            classes={{
              title: classes.title,
              action: classes.action
            }}
            title="Links Management"
          />
          <CardContent className={classes.cardContent}>
            <ReactTable
              className={cx(classes.table, '-striped -highlight')}
              data={links}
              columns={columns}
              defaultPageSize={20}
              defaultFilterMethod={defaultFilter}
              filterable={true}
              filtered={filters}
              onFilteredChange={this.handleFilterChange}
              loading={isFetching}
              LoadingComponent={Loading}
            />
          </CardContent>
        </Card>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    links: getLinksWithCurrentSettings(state),
    isFetching: state.analyzer.isFetching
  }
};

const mapDispatchToProps = dispatch => {
  return {
    getClientLinks: () => dispatch(getClientLinksRequest()),
    getMargin: () => dispatch(getSettingsRequest()),
    deleteLinks: ids => dispatch(deleteLinksRequest(ids))
  }
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(LinksManagement));

