import React, { Component } from 'react'

import ReactTable from 'react-table'
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';

import cx from 'classnames';
import queryString from 'stringquery'
import { connect } from 'react-redux'
import Loading from '../../../../base-components/Loading/Loading'
import { addEditorRequest, getEditorsRequest } from '../../actions'
import { getEditorsWithUrl } from '../../reducers'

import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

import styles from '../../../../assets/jss/react-table'
import pageStyles from './styles'

class EditorsManagement extends Component {

  state = {
    editorName: ''
  }

  componentDidMount() {
    this.props.getEditorsRequest();
  }

  handleFilterChange = (filters) => {
    const searchParams = new URLSearchParams();
    filters.map(filter => searchParams.set(filter.id, filter.value));
    this.props.history.push(`?${searchParams}`);
  };

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  addEditor = e => {
    e.preventDefault();
    try {
      this.props.addEditorRequest(this.state.editorName);
      NotificationManager.success('Editor added successfully', 'Success!', 2000);
      this.setState({ editorName: '' });
    } catch (e) {
      NotificationManager.error('Failed to add editor', 2000);
    }
  }

  handleCopyClick = url => {
    const el = document.createElement('textarea');
    el.value = url;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    NotificationManager.success('URL copied to clipboard', 'Success!', 1000);
  }

  render() {
    const { editors, isFetching, classes } = this.props;
    const columns = [{
      Header: 'Editor Name',
      accessor: 'name',
      width: 400
    }, {
      Header: 'URL',
      accessor: 'url',
    }, {
      Header: '',
      accessor: 'url',
      sortable: false,
      filterable: false,
      width: 200,
      Cell: props => (
        <div>
          <Button variant="contained"
            onClick={() => this.handleCopyClick(props.value)}
          >Copy URL</Button>
        </div>
      )
    }
    ];

    const values = Object.values(queryString(this.props.location.search));
    const keys = Object.keys(queryString(this.props.location.search));

    const filters = keys.map((key, index) => {
      return {
        id: key,
        value: decodeURIComponent(values[index])
      };
    });

    return (
      <div className={classes.container}>
        <NotificationContainer />
        <Card className={classes.card}>
          <CardHeader
            className={classes.cardHeader}
            action={
              <form className={classes.form} noValidate autoComplete="off" onSubmit={this.addEditor}>
                <TextField
                  id="editor-name"
                  label="Editor name"
                  value={this.state.editorName}
                  onChange={this.handleChange('editorName')}
                  className={classes.textfield}
                  InputLabelProps={{
                    classes: {
                      root: classes.cssLabel,
                      focused: classes.cssFocused
                    },
                  }}
                  InputProps={{
                    classes: {
                      underline: classes.cssUnderline,
                      input: classes.input
                    },
                  }}
                  margin="normal"
                />
                <Button variant="contained" color="secondary" type="submit">
                  Add new editor
                </Button>
              </form>

            }
            classes={{
              title: classes.title,
              action: classes.action
            }}
            title="Editors Management"
          />
          <CardContent className={classes.cardContent}>
            <ReactTable
              className={cx(classes.table, '-striped -highlight')}
              data={editors}
              columns={columns}
              defaultPageSize={20}
              defaultFilterMethod={(filter, row) =>
                String(row[filter.id].toLocaleLowerCase()).indexOf(filter.value.toLocaleLowerCase()) > -1}
              filterable={true}
              filtered={filters}
              onFilteredChange={this.handleFilterChange}
              loading={isFetching}
              LoadingComponent={Loading}
            />
          </CardContent>
        </Card>
      </div>
    )
  }
}

const mapStateToProps = state => ({ editors: getEditorsWithUrl(state) })

export default withStyles(theme => ({
  ...styles(theme),
  ...pageStyles(theme)
}))(connect(mapStateToProps, { addEditorRequest, getEditorsRequest })(EditorsManagement));

