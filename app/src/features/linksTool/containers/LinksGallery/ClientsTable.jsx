import React, { Component } from 'react'
import ReactTable from 'react-table'
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import cx from 'classnames';

import queryString from 'stringquery'
import { connect } from 'react-redux'
import { getClientLinksRequest } from '../../actions'
import { getLinksWithCurrentSettings } from '../../reducers'
import Loading from '../../../../base-components/Loading/Loading'
import columnDefs from '../../../../base-components/LinkTable/columns';
import styles from '../../../../assets/jss/react-table'
import { defaultFilter } from '../../../../helpers/reactTableHelper';

class ClientsTable extends Component {

    componentDidMount() {
        this.props.getClientLinks({ onlyLowest: true });
    }

    handleFilterChange = (filters) => {
        const searchParams = new URLSearchParams();
        filters.map(filter => searchParams.set(filter.id, filter.value));

        this.props.history.push(`?${searchParams}`);
    };


    render() {
        const { links, isFetching, classes } = this.props;
        const columns = [
            columnDefs.getStringColumn({
                name: 'Domain URL',
                accessor: 'url',
                width: 300,
            }),
            columnDefs.getNumberColumn({
                name: 'TAT',
                id: 'tat',
                min: 0,
                max: 120,
            }),
            columnDefs.getSelectColumn({
                name: 'Link Type',
                accessor: 'link',
                options: ['Dofollow', 'Nofollow'],
                getProps: (state, rowInfo, column) => {
                  return {
                    style: {
                      textDecoration: rowInfo && rowInfo.row && rowInfo.row.link && rowInfo.row.link.toLowerCase().trim() === 'nofollow' ? 'line-through' : 'none',
                    },
                  };
                },
            }),
            columnDefs.getStringColumn({
                name: 'Anchor Type',
                accessor: 'anchor',
            }),
            columnDefs.getNumberColumn({
                name: 'Price ($)',
                id: 'convertedPrice',
                min: 0,
                max: 10000,
            }),
            columnDefs.getStringColumn({
                name: 'Notes',
                accessor: 'notes',
            }),
        ];

        const values = Object.values(queryString(this.props.location.search));
        const keys = Object.keys(queryString(this.props.location.search));
        const filters = keys.map((key, index) => {
            return {
                id: key,
                value: decodeURIComponent(values[index])
            };
        });

        return (
            <div className={classes.container}>
                <Card className={classes.card}>
                    <CardContent className={classes.cardContent} style={{ height: '100%' }}>
                        <ReactTable
                            className={cx(classes.table, '-striped -highlight')}
                            data={links}
                            columns={columns}
                            defaultPageSize={20}
                            defaultFilterMethod={defaultFilter}
                            filterable={true}
                            filtered={filters}
                            onFilteredChange={this.handleFilterChange}
                            loading={isFetching}
                            LoadingComponent={Loading}
                        />
                    </CardContent>
                </Card>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        links: getLinksWithCurrentSettings(state),
        isFetching: state.analyzer.isFetching
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getClientLinks: (queries) => dispatch(getClientLinksRequest(queries))
    }
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(ClientsTable));
