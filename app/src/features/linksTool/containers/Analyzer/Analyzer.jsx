import React, { Component } from 'react'
import { connect } from 'react-redux'
import { scrapeGoogleRequest }  from '../../actions'

class Analyzer extends Component {

    state = {
        keyword: '',
    };

    handleKeywordChange = (keyword) => {
        this.setState({ keyword });
    };

    renderSerp = () => {
        const { analysis } = this.props;
        // const serp = {"serp":[{"url":"https://www.ssa.gov/cola/","body":{"exactKeywordsNumber":34,"exactKeywordsDensity":0.011556764106050306}},{"url":"https://en.wikipedia.org/wiki/Cola","body":{"exactKeywordsNumber":137,"exactKeywordsDensity":0.05959112657677251}},{"url":"http://www.cola.org/","body":{"exactKeywordsNumber":26,"exactKeywordsDensity":0.018258426966292134}},{"url":"https://www.investopedia.com/terms/c/cola.asp","body":{"exactKeywordsNumber":29,"exactKeywordsDensity":0.029531568228105907}},{"url":"https://www.coca-colacompany.com/","body":{"exactKeywordsNumber":77,"exactKeywordsDensity":0.002154388517388993}},{"url":"http://www.coca-cola.com/global/","body":{"exactKeywordsNumber":4,"exactKeywordsDensity":0.001851851851851852}},{"url":"https://us.coca-cola.com/","body":{"exactKeywordsNumber":43,"exactKeywordsDensity":0.0008073146461896626}},{"url":"https://en.wiktionary.org/wiki/cola","body":{"exactKeywordsNumber":71,"exactKeywordsDensity":0.057676685621445976}},{"url":"https://variety.com/2019/tv/news/coca-cola-pulls-super-bowl-commercials-2019-1203116744/","body":{"exactKeywordsNumber":22,"exactKeywordsDensity":0.0012091898428053204}},{"url":"https://www.ttb.gov/labeling/colas.shtml","body":{"exactKeywordsNumber":27,"exactKeywordsDensity":0.012587412587412588}}],"exactKeywordsCorrelation":-0.32051660019341854,"exactKeywordsDensityCorrelation":-0.23504973462347975};
        // console.log(serp);
        if (!analysis) return <div></div>;
        return (
            <div style={{marginLeft: 50, marginTop: 50}}>
                <h2>Keyword: {this.state.keyword}</h2>
                <h4>Exact Keyword Correlation: <b>{`${analysis.exactKeywordsCorrelation.toFixed(2) * 100}%`}</b></h4>
                <h4>Exact Keyword Density Correlation: <b>{`${analysis.exactKeywordsDensityCorrelation.toFixed(2) * 100}%`}</b></h4>
                <br />
                <h4>SERP(body tag): </h4>
                <ol>
                    {
                        analysis.serp.map((item, index) => (
                            <li key={index} style={{marginBottom: 10}}>
                                <div>{item.url}</div>
                                <div>
                                    <small>Exact Keywords: <b style={{fontSize: 14}}>{item.body.exactKeywordsNumber}</b></small>
                                </div>
                                <div>
                                    <small>Exact Keywords Density: <b style={{fontSize: 14}}>{item.body.exactKeywordsDensity}</b></small>
                                </div>
                            </li>
                        ))
                    }
                </ol>
            </div>
        )
    };

    render() {
        const { scrapeGoogle, isFetching } = this.props;

        return (
            <div style={{margin: '0 auto', width: 900}}>
                <form style={{margin: '0 auto', width: 400, marginTop: 100}} onSubmit={(e) => {
                    e.preventDefault();
                    scrapeGoogle(this.state.keyword);
                }}>
                    <input
                        style={{
                            width: 200,
                            height: 40,
                            borderRadius: 3,
                            border: '1px solid grey',
                            paddingLeft: 10
                        }}
                        type='text' placeholder={'Keyword'} value={this.state.keyword} onChange={e => this.handleKeywordChange(e.target.value)}/>
                    <input style={{height: 40, marginLeft: 10}} type='submit' disabled={isFetching} value={isFetching ? 'Analyzing...' : 'Submit'}/>
                </form>

                {this.renderSerp()}
            </div>)
    }
}

const mapStateToProps = state => {
    return {
        serp: state.analyzer.serp,
        analysis: state.analyzer.analysis,
        isFetching: state.analyzer.isFetching,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        scrapeGoogle: keyword => dispatch(scrapeGoogleRequest(keyword))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Analyzer);
