import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';
import { TextField } from 'formik-material-ui';

import isEqual from 'lodash/isEqual';
import sortBy from 'lodash/sortBy';
import getSupportedHeaders from '../../../../helpers/csvHeaderMapping';

const generateMapping = headers => {
  const mapping = {};
  getSupportedHeaders().forEach(header => {
    const { name } = header;
    mapping[name] = headers.includes(name) ? name : ''
  })
  return mapping;
}

const generateSchema = () => {
  const schema = {};
  getSupportedHeaders().forEach(header => {
    const { name, required } = header;
    if(required) {
      schema[name] = Yup.string().required('This field is required');
    }
  });
  return Yup.object().shape(schema);
}

class HeaderMappingForm extends Component {
  state = {
    mapping: generateMapping(this.props.userHeaders),
    validationSchema: generateSchema()
  }

  static getDerivedStateFromProps(props, state) {
    if (!isEqual(sortBy(props.userHeaders), sortBy(state.userHeaders))) {
      return {
        mapping: generateMapping(props.userHeaders),
        userHeaders: props.userHeaders,
      };
    }
    return null;
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (!isEqual(sortBy(this.props.userHeaders), sortBy(nextProps.userHeaders)) ||
      !isEqual(this.state.mapping, nextState.mapping));
  }

  renderUserHeadersDropdown = () => {
    return getSupportedHeaders().map(this.renderDropdown)
  }

  renderDropdown = ({ name: header }) => (
    <Grid key={header} item xs={4}>
      <Field
        className={this.props.classes.formControl}
        type="text"
        name={header}
        label={header}
        select
        margin="normal"
        component={TextField}
      >
        { this.props.userHeaders.map(header => <MenuItem key={header} value={header}>{header}</MenuItem>) }
      </Field>
    </Grid>
  )

  renderCurrencyDropdown = () => (
    <Grid item xs={4}>
      <Field
        className={this.props.classes.formControl}
        type="text"
        name='currency'
        label='Currency'
        select
        margin="normal"
        component={TextField}
      >
        <MenuItem value="USD">USD</MenuItem>
        <MenuItem value="EUR">EUR</MenuItem>
      </Field>
    </Grid>
  )

  render() {
    const { classes, onSubmit } = this.props;
    return (
      <Formik
        enableReinitialize={true}
        initialValues={{...this.state.mapping, currency: 'USD'}}
        onSubmit={(values, { setSubmitting }) => {
          onSubmit(objectFlip(values));
          setSubmitting(false);
        }}
        validationSchema={this.state.validationSchema}
      >
        {props => {
          return (
            <Form className={classes.root}>
              <Grid container spacing={24}>
                {this.renderUserHeadersDropdown()}
                {this.renderCurrencyDropdown()}
              </Grid>
              <Button
                variant='contained'
                className={classes.submitBtn}
                type='submit'> Submit </Button>
            </Form>
          )}}
      </Formik>
    )
  }
}

const objectFlip = obj => {
  const ret = {};
  Object.keys(obj).forEach((key) => {
    if(key !== 'currency') {
      ret[obj[key]] = key;
    } else {
      ret[key] = obj[key];
    }
  });
  return ret;
}

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    padding: '5px 30px 20px 20px',
    justifyContent: 'center'
  },
  formControl: {
    margin: theme.spacing.unit,
    width: '100%'
  },
  submitBtn: {
    marginTop: 30
  }
})

export default withStyles(styles)(HeaderMappingForm);