import React, { Component, Fragment } from 'react'
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { Button, Card } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';

import { addLinksRequest } from '../../actions'
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import HeaderMappingForm from './HeaderMappingForm';
import { getEditor } from '../../../../services/editors';
import SpinnerWithBackdrop from '../../../../base-components/SpinnerWithBackdrop/SpinnerWithBackdrop';

let fileReader;

class ImportEditor extends Component {

    state = {
        editorLinks: [],
        csvHeaders: [],
        filename: '',
        editor: null,
        error: null,
    };

    async componentDidMount() {
        const editorId = this.props.match.params.editorId;
        try {
            const editor = await getEditor(editorId);
            this.setState({ editor });
        } catch (e) {
            this.setState({ error: e, editor: {} })
        }
    }

    handleFileRead = (e) => {
        const content = fileReader.result;
        const editorLinks = csvJSON(content);
        try {
            const csvHeaders = extractHeaders(editorLinks[0]);
            this.setState({ editorLinks, csvHeaders });
        } catch (e) {
            this.setState({ error: e })
        }
    };

    handleFileChosen = (file) => {
        fileReader = new FileReader();
        fileReader.onloadend = this.handleFileRead;
        fileReader.readAsText(file);
        this.setState({ filename: file.name });
    };

    handleSubmit = mapping => {
        const editorId = this.props.match.params.editorId;
        const convertedLinks = convertLinksToSupportedHeaders(this.state.editorLinks, mapping, editorId);
        this.props.addLinksRequest(convertedLinks);
        NotificationManager.success('Csv has been uploaded!', 'Success!', 2000);
    }

    render() {
        const { classes, history, match } = this.props;
        const { csvHeaders, filename, editor, error } = this.state;

        if (editor === null) return <SpinnerWithBackdrop />

        const cardContent = error ? <p className={classes.warning}>Editor import link is not valid, please ask for a valid link</p> :
            <Fragment>
                <CardHeader
                    className={classes.cardHeader}
                    classes={{
                        title: classes.title
                    }}
                    title={`Hi ${editor.name}`}
                />
                <CardContent className={classes.cardContent}>
                    <p>Please, upload your CSV file with links by clicking on the button below.</p>
                    <form className={classes.uploadForm}>
                        <input
                            accept="text/csv"
                            style={{ display: 'none' }}
                            id="contained-button-file"
                            onChange={e => this.handleFileChosen(e.target.files[0])}
                            // Fixed issue when user select same file but have different content onChange didn't fired
                            onClick={e => { e.target.value = null }}
                            type="file"
                        />
                        <label htmlFor="contained-button-file">
                            <Button variant="contained" component="span">
                                Upload CSV with links
                            </Button>
                        </label>
                        <p>{filename}</p>
                    </form>
                    <p>Or, you can update existing links by clicking on the button below.</p>
                    <Button variant="contained" onClick={() => history.push('/links-management/' + match.params.editorId)}>
                        Update your links
                    </Button>
                </CardContent>
            </Fragment>;

        return <div className={classes.container}>
            <Card className={classes.card}>
                {cardContent}
                <NotificationContainer />
            </Card>
            {csvHeaders.length > 0 && <Card className={classes.card}>
                <CardHeader
                    className={classes.cardHeader}
                    classes={{
                        title: classes.title
                    }}
                    title={`Match the columns from the imported file to ${editor.name}'s contact fields`}
                />
                <CardContent>
                    <HeaderMappingForm userHeaders={csvHeaders} onSubmit={mapping => this.handleSubmit(mapping)} />
                </CardContent>
            </Card>}

        </div>
    }
}

function csvJSON(csv) {
    let lines = csv.replace(/\n/g, "\r").split("\r");
    let result = [];
    let headers = lines[0].split(",");

    for (let i = 1; i < lines.length; i++) {
        let obj = {};
        let currentline = lines[i].split(",");

        for (let j = 0; j < headers.length; j++) {
            obj[headers[j]] = currentline[j];
        }
        result.push(obj);
    }
    return result;
}

const extractHeaders = link => Object.keys(link);

const convertLinksToSupportedHeaders = (links, mapping, editorId) => {
    return links.map(link => {
        const convertedLink = {};
        Object.keys(link).forEach(key => {
            convertedLink[mapping[key]] = link[key]
        })
        convertedLink.editor = editorId;
        convertedLink.currency = mapping.currency;
        return convertedLink;
    });
}

const mapDispatchToProps = dispatch => {
    return {
        addLinksRequest: links => dispatch(addLinksRequest(links))
    }
};

const styles = styles => ({
    container: {
        margin: '0 auto',
        width: 900
    },
    warning: {
        color: 'red',
        textAlign: 'center'
    },
    card: {
        marginTop: 30
    },
    cardHeader: {
        backgroundColor: '#180561',
        height: '10%',
    },
    cardContent: {
        padding: '20px 70px',
        fontSize: '15px',
        fontWeight: 300,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        color: '#fff',
        fontWeight: 300,
        fontSize: 15
    },
    uploadForm: {
        margin: '0 auto',
        width: 400,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default compose(
    withRouter,
    withStyles(styles),
    connect(null, mapDispatchToProps),
)(ImportEditor);
