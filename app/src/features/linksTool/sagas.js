import { call, put, takeEvery } from 'redux-saga/effects';
import config from '../../config';
import * as constants from './constants';
import * as actions from './actions';
import req from '../../helpers/request';

export function* scrapeGoogle({ keyword }) {
    const url = config[process.env.REACT_APP_ENV].PARSER_DOMAIN + '/scrape';
    const response = yield call(req, url, 'POST', { queries: [ keyword ] });
    const body = yield response.json();

    if (response.ok) {
        yield put(actions.scrapeGoogleSuccess(body));
        const links = body.res.results[0].links.map(model => model.link);
        yield put(actions.scrapeURLRequest(links, keyword))
    } else {
        throw response;
    }
}

export function* scrapeURL({ links, keyword }) {
    const domain = config[process.env.REACT_APP_ENV].API_DOMAIN;
    const route = domain + '/analyze/scrapeURL';
    const response = yield call(req, route, 'POST', { links, keyword });
    const body = yield response.json();

    if (response.ok) {
        yield put(actions.scrapeURLSuccess(body));
    } else {
        throw response;
    }
}

export function* getClientLinks({ queries }) {
    const domain = config[process.env.REACT_APP_ENV].API_DOMAIN;
    let route = domain + `/links?onlyLowest=${queries.onlyLowest || false}`;
    if(queries.editor) route += `&editor=${queries.editor}`;
    const response = yield call(req, route, 'GET');
    const body = yield response.json();

    if (response.ok) {
        // const parseCsv = Papa.parse(body.csv, {header: true});
        yield put(actions.getClientLinksSuccess(body));
    } else {
        throw response;
    }
}

export function* addLinks({ links }) {
    const domain = config[process.env.REACT_APP_ENV].API_DOMAIN;
    const route = domain + '/links';
    const response = yield call(req, route, 'POST', { links });

    if (response.ok) {
        yield put(actions.addLinksSuccess());
    } else {
        throw response;
    }
}

export function* createSettings({margin, fee}) {
    const url = config[process.env.REACT_APP_ENV].API_DOMAIN + '/settings';
    const response = yield call(req, url, 'POST', margin ? {margin, fee}: {fee});
    const body = yield response.json();

    if (response.ok) {
        yield put(actions.createSettingsSuccess(body.margin, body.fee));
    } else {
        throw response;
    }
}

export function* getSettings() {
    const url = config[process.env.REACT_APP_ENV].API_DOMAIN + '/settings';
    const response = yield call(req, url, 'GET');
    const {margin, fee} = yield response.json();

    if (response.ok) {
        yield put(actions.getSettingsSuccess(margin, fee));
    } else {
        throw response;
    }
}

export function* deleteLinks({ids}) {
    const domain = config[process.env.REACT_APP_ENV].API_DOMAIN;
    const route = domain + '/links/delete-bulk';
    const response = yield call(req, route, 'PUT', { ids });
    const { ids: deletedIds } = yield response.json();
    if (response.ok) {
        yield put(actions.deleteLinksSuccess(deletedIds));
    } else {
        throw response;
    }
}

export function* addEditor({ name }) {
    const domain = config[process.env.REACT_APP_ENV].API_DOMAIN;
    const route = domain + '/editors';
    const response = yield call(req, route, 'POST', { name });
    const data = yield response.json();
    if (response.ok) {
        yield put(actions.addEditorSuccess(data));
    } else {
        throw response;
    }
}

export function* getEditors() {
    const domain = config[process.env.REACT_APP_ENV].API_DOMAIN;
    const route = domain + '/editors';
    const response = yield call(req, route, 'GET');
    const body = yield response.json();

    if (response.ok) {
        yield put(actions.getEditorsSuccess(body));
    } else {
        throw response;
    }
}

export function* updateLink({ link }) {
    const domain = config[process.env.REACT_APP_ENV].API_DOMAIN;
    const route = domain + '/links/' + link._id;
    const response = yield call(req, route, 'PUT', { link });
    const data = yield response.json();
    if (response.ok) {
        yield put(actions.updateLinkSuccess(data));
    } else {
        throw response;
    }
}

export default function* () {
    yield takeEvery(constants.SCRAPE_GOOGLE_REQUEST, scrapeGoogle);
    yield takeEvery(constants.SCRAPE_URL_REQUEST, scrapeURL);
    yield takeEvery(constants.GET_CLIENT_LINKS_REQUEST, getClientLinks);
    yield takeEvery(constants.ADD_LINKS_REQUEST, addLinks);
    yield takeEvery(constants.CREATE_SETTINGS_REQUEST, createSettings);
    yield takeEvery(constants.GET_SETTINGS_REQUEST, getSettings);
    yield takeEvery(constants.DELETE_LINKS_REQUEST, deleteLinks);
    yield takeEvery(constants.ADD_EDITOR_REQUEST, addEditor);
    yield takeEvery(constants.GET_EDITORS_REQUEST, getEditors);
    yield takeEvery(constants.UPDATE_LINK_REQUEST, updateLink);
}
