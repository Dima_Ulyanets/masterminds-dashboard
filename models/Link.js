const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const linkSchema = new Schema({
    editor: { type: Schema.Types.ObjectId, ref: 'Editor', required: true },
    url: {type: String, required: true},
    tat: {type: Number, required: false},
    link: {type: String, required: true},
    anchor: {type: String, required: false},
    price: {type: Number, required: true},
    currency: {type: String, required: true},
    notes: {type: String, required: false},
});

linkSchema.statics = {
    getById(id) {
        return this.findOne({_id: id})
            .exec()
    }
};

linkSchema.index({ editor: 1, url: 1, link: 1}, { unique: true });
module.exports = mongoose.model('Link', linkSchema);
