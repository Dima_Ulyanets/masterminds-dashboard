const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const marginSchema = new Schema({
    margin: {type: Number, required: true},
}, {timestamps: true});


module.exports = mongoose.model('Margin', marginSchema);
