const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const feeSchema = new Schema({
    fee: {type: Number, required: true},
}, {timestamps: true});


module.exports = mongoose.model('Fee', feeSchema);
