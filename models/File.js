const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const fileSchema = new Schema({
    url: {type: String, required: true},
    name: {type: String, required: true},
});

fileSchema.statics = {
    getById(id) {
        return this.findOne({_id: id})
            .exec()
    }
};

module.exports = mongoose.model('File', fileSchema);
