require('dotenv').config();
require('./jobs/index');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const mongoose = require('mongoose');
const boom = require('boom');
const router = require('./routes/index');
const fileUpload = require('express-fileupload');

const app = express();
const http = require('http').Server(app);
mongoose.Promise = Promise;
mongoose.connect(process.env.MONGODB_URL);

app.use(fileUpload());
app.use(cors());

app.use(bodyParser.json({limit: '50mb'}));
app.use('/api', router);
app.use('/static', express.static(path.join(__dirname, 'public')));
app.use('/', express.static(path.join(__dirname, 'app/build')));
app.get('*', function (request, response) {
    response.sendFile(path.resolve(__dirname, 'app/build/index.html'));
});

app.use((req, res, next) => {
    throw boom.notFound('Not found');
});

app.use((err, req, res, next) => {
    console.log(err);
    throw err;
});

http.listen(process.env.PORT);
