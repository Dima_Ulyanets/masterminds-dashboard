const axios = require('axios');
const exchangeEndpoint = 'https://free.currencyconverterapi.com/api/v6/convert?q=USD_EUR&compact=ultra&apiKey=5e4d6944a20f7e6df753';

async function getUSDEURExchangeRate() {
    try {
        const rate = await axios.get(exchangeEndpoint).then(response => {
            return response.data.USD_EUR;
        }).catch(e => {
            console.log(e);
        });
        return rate;
    } catch(e) {
        console.log(e);
        return false;
    }

}

module.exports = {
    getUSDEURExchangeRate,
};
