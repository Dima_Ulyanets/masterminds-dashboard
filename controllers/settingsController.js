const Margin = require('../models/Margin');
const Fee = require('../models/Fee');

async function getLatestSettings(req, res) {
    try {
        const marginReq = Margin.findOne().sort({createdAt: -1});
        const feeReq = Fee.findOne().sort({createdAt: -1});
        const [{margin}, {fee}] = await Promise.all([marginReq, feeReq]);
        res.send({margin, fee});
    } catch (e) {
        console.log(e);
        return false;
    }
}

async function createSettings(req, res) {
    try {
        const marginReq = (req.body.margin !== undefined) ? Margin.create({ margin: req.body.margin }) : 0;
        const feeReq = (req.body.fee !== undefined) ? Fee.create({ fee: req.body.fee }) : 0;
        const [{margin}, {fee}] = await Promise.all([marginReq, feeReq]);
        res.send({margin, fee});
    } catch (e) {
        console.log(e);
        return false;
    }
}

module.exports = {
    getLatestSettings,
    createSettings
};
