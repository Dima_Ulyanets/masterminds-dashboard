const Editor = require('../models/Editor');

async function getEditors(req, res) {
    try {
        const editors = await Editor.find();
        res.send(editors);
    } catch (e) {
        console.log(e);
        res.status(400).send(e);
    }
}

async function getEditor(req, res) {
    try {
        const editor = await Editor.findById(req.params.id);
        res.send(editor);
    } catch (e) {
        console.log(e);
        res.status(400).send(e);
    }
}

async function createEditor(req, res) {
    try {
        const editor = await Editor.create({name: req.body.name});
        res.send(editor);
    } catch (e) {
        console.log(e);
        res.status(400).send(e);
    }
}

module.exports = {
    getEditors,
    getEditor,
    createEditor
};
