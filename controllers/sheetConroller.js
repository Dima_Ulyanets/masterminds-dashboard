const axios = require('axios');
const Link = require('../models/Link');
const Querystring = require('querystring');
const ExchangeController = require('./exchangeConroller');
const Margin = require('../models/Margin');
const Fee = require('../models/Fee');
const uniqBy = require('lodash/uniqBy');
const sortBy = require('lodash/sortBy');

const ZohoSheetApi = 'https://sheet.zoho.com/api/v2';
const InternalSpreadsheetId = 'b4i02482af96b1ce248d1bbb24cade0d83386';
const ClientSpreadsheetId = 'cfntz290cde7c6945407aa8da7793dd235cf2';
const AuthorizationParam = '888b444529e9669caea1bf25a093b45d';
const mongoose = require('mongoose');

async function getMargin() {
    const settingsWorkbookId = 'cdmb98af1582123f24943ade13b6792bd1cfb';
    const link = `${ZohoSheetApi}/${settingsWorkbookId}?authtoken=${AuthorizationParam}`;
    try {
        const config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
        };
        const requestBody = Querystring.stringify({
            method: 'cell.content.get',
            worksheet_name: 'Sheet1',
            row: 2,
            column: 1,
        });
        const margin = await axios.post(link, requestBody, config).then(response => {
            return response.data.content;
        }).catch(e => {
            console.log(e);
        });
        return margin;
    } catch(e) {
        console.log(e);
        return false;
    }

}

async function getFee() {
    const settingsWorkbookId = 'cdmb98af1582123f24943ade13b6792bd1cfb';
    const link = `${ZohoSheetApi}/${settingsWorkbookId}?authtoken=${AuthorizationParam}`;
    try {
        const config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
        };
        const requestBody = Querystring.stringify({
            method: 'cell.content.get',
            worksheet_name: 'Sheet1',
            row: 2,
            column: 2,
        });
        const fee = await axios.post(link, requestBody, config).then(response => {
            return response.data.content;
        }).catch(e => {
            console.log(e);
        });
        return fee;
    } catch(e) {
        console.log(e);
        return false;
    }

}

async function getAllWorkbooks() {
    const link = `${ZohoSheetApi}/workbooks?authtoken=${AuthorizationParam}`;
    try {
        const config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
        };
        const requestBody = Querystring.stringify({
            method: 'workbook.list',
        });
        const response = await axios.post(link, requestBody, config).then(response => {
            return response.data.workbooks;
        }).catch(e => {
            console.log(e);
        });
        return response;
    } catch(e) {
        console.log(e);
        return false;
    }

}

async function getSpreadsheetData(workbookId) {
    const link = `${ZohoSheetApi}/${workbookId}?authtoken=${AuthorizationParam}`;
    try {
        const config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
        };
        const requestBody = Querystring.stringify({
            method: 'range.content.get',
            worksheet_name: 'Sheet1',
            start_row: 2,
            end_row: 256,
            start_column: 1,
            end_column: 6,
        });
        const response = await axios.post(link, requestBody, config).then(response => {
            return response.data.range_details;
        }).catch(e => {
            console.log(e);
        });
        return response;
    } catch(e) {
        console.log(e);
        return false;
    }
}

async function clearSpreadsheet(workbookId) {
    const link = `${ZohoSheetApi}/${workbookId}?authtoken=${AuthorizationParam}`;
    try {
        const config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
        };
        const requestBody = Querystring.stringify({
            method: 'namedrange.delete',
            name_of_range: 'internal_range',
            start_row: 2,
            end_row: 256,
            start_column: 1,
            end_column: 256,
        });
        const response = await axios.post(link, requestBody, config).then(response => {
            return response.data.range_details;
        }).catch(e => {
            console.log(e);
        });
        return response;
    } catch(e) {
        console.log(e);
        return false;
    }
}

async function updateInternalSpreadsheet(workbookId, data) {
    const link = `${ZohoSheetApi}/${workbookId}?authtoken=${AuthorizationParam}`;
    try {
        const config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
        };
        const requestBody = Querystring.stringify({
            method: 'worksheet.csvdata.set',
            worksheet_name: 'Sheet1',
            row: 2,
            column: 1,
            data
        });
        const response = await axios.post(link, requestBody, config).then(response => {
            return response.data;
        }).catch(e => {
            console.log(e);
        });
        return {
            result: response,
        };
    } catch(e) {
        console.log(e);
        return false;
    }
}

async function generateInternalSpreadsheet() {
    const exchangeRate = await ExchangeController.getUSDEURExchangeRate() || 1;
    const records = [];
    const margin = await getMargin();
    const fee = await getFee();
    const workbooks = await getAllWorkbooks();
    const editorWorkbooks = filterEditorsWorkbooks(workbooks);
    for (let i = 0; i < editorWorkbooks.length; i ++) {
        const spreadsheet = await getSpreadsheetData(editorWorkbooks[i].resource_id);
        for (let d = 0; d < spreadsheet.length; d ++) {
            const editorRow = spreadsheet[d].row_details.map((details, index) => {
                if (editorWorkbooks[i].currency === 'EUR' && index === 1) {
                    return Math.ceil(details.content * exchangeRate);
                }
                return details.content;
            });
            let record = editorWorkbooks[i].workbook_name.replace('Editor', '');
            record = record.replace('(EUR)', '');
            editorRow.unshift(record);
            records.push(editorRow);
        }
    }
    const sortedRecordsByLinkAndPrice = records.sort(function (record1, record2) {
        if (record1[1] > record2[1]) return 1;
        if (record1[1] < record2[1]) return -1;

        if (parseInt(record1[2]) < parseInt(record2[2])) return -1;
        if (parseInt(record1[2]) > parseInt(record2[2])) return 1;

    });

    let csvData = '';
    for (let b = 0; b < sortedRecordsByLinkAndPrice.length; b ++) {
        const profit = (parseInt(sortedRecordsByLinkAndPrice[b][2]) * margin) - sortedRecordsByLinkAndPrice[b][2];
        const netProfit = profit - (profit * fee / 100);
        csvData += `${sortedRecordsByLinkAndPrice[b][0]},`;
        csvData += `${sortedRecordsByLinkAndPrice[b][1]},`;
        csvData += `${sortedRecordsByLinkAndPrice[b][2]},`;
        csvData += `${sortedRecordsByLinkAndPrice[b][3]},`;
        csvData += `${sortedRecordsByLinkAndPrice[b][4]},`;
        csvData += `${sortedRecordsByLinkAndPrice[b][5]},`;
        csvData += `${parseInt(sortedRecordsByLinkAndPrice[b][2]) * margin},`;
        csvData += `${profit},`;
        csvData += `${netProfit},`;
        csvData += `${sortedRecordsByLinkAndPrice[b][6] || '-'}\r\n`;
    }
    updateInternalSpreadsheet(InternalSpreadsheetId, csvData)
}

async function generateClientLinks() {
    const exchangeRate = await ExchangeController.getUSDEURExchangeRate() || 1;
    const records = [];
    const margin = await getMargin();
    const workbooks = await getAllWorkbooks();
    const editorWorkbooks = filterEditorsWorkbooks(workbooks);
    for (let i = 0; i < editorWorkbooks.length; i ++) {
        const spreadsheet = await getSpreadsheetData(editorWorkbooks[i].resource_id);
        for (let d = 0; d < spreadsheet.length; d ++) {
            const editorRow = spreadsheet[d].row_details.map((details, index) => {
                if (editorWorkbooks[i].currency === 'EUR' && index === 1) {
                    return Math.ceil(details.content * exchangeRate);
                }
                return details.content;
            });
            let record = editorWorkbooks[i].workbook_name.replace('Editor', '');
            record = record.replace('(EUR)', '');
            editorRow.unshift(record);
            records.push(editorRow);
        }
    }
    const sortedRecordsByProfit = records.sort(function (record1, record2) {
        const profit1 = (parseInt(record1[2]) * margin) - record1[2];
        const profit2 = (parseInt(record2[2]) * margin) - record2[2];
        if (parseInt(profit1) < parseInt(profit2)) return -1;
        if (parseInt(profit1) > parseInt(profit2)) return 1;

    });

    const tempArray = [];
    const uniqueRecords = sortedRecordsByProfit.filter(record => {
        if (tempArray.indexOf(record[1]) > - 1) {
            return false;
        } else {
            tempArray.push(record[1]);
            return true;
        }
    });
    let csvData = '';
    for (let b = 0; b < uniqueRecords.length; b ++) {
        csvData += `${uniqueRecords[b][0]},`;
        csvData += `${uniqueRecords[b][1]},`;
        csvData += `${uniqueRecords[b][3]},`;
        csvData += `${uniqueRecords[b][4]},`;
        csvData += `${uniqueRecords[b][5]},`;
        csvData += `${parseInt(uniqueRecords[b][2]) * margin},`;
        csvData += `${uniqueRecords[b][6] || '-'}\r\n`;
    }

    return csvData;
}

async function generateClientSpreadsheet() {
    const csvData = await generateClientLinks();
    updateInternalSpreadsheet(ClientSpreadsheetId, csvData);
}

async function getClientLinks(req, res) {
    const getRateReq = ExchangeController.getUSDEURExchangeRate();
    const queries = req.query.editor ? {
        editor: mongoose.Types.ObjectId(req.query.editor),
    } : {};
    const getLinksReq = Link.find(queries).populate('editor', 'name');
    const getMarginReq = Margin.findOne().sort({createdAt: -1});
    const getFeeReq = Fee.findOne().sort({createdAt: -1});
    const [rate, links, { margin }, { fee }] = await Promise.all([getRateReq, getLinksReq, getMarginReq, getFeeReq]);
    let convertedLinks = links.map(link => updatePriceBasedOnCurrency(link, rate));
    if(req.query.onlyLowest === 'true') {
        convertedLinks = filterLowestPrice(convertedLinks);
    }
    return res.send({links: convertedLinks, margin, fee});
}

function filterLowestPrice(links) {
    const sortedByLowestPrice = sortBy(links, link => link.convertedPrice);
    const uniqueLinksWithLowestPrice = uniqBy(sortedByLowestPrice, link => [link.url, link.link].join());
    return uniqueLinksWithLowestPrice;
}

function updatePriceBasedOnCurrency(link, rate) {
    const { currency, price } = link._doc;
    return {...link._doc, convertedPrice: currency === 'EUR' ? convertToUSD(price, rate) : price};
}

function convertToUSD(euro, rate) {
    return Math.round( euro / rate * 10 ) / 10;
}

async function addLinks(req, res) {
    const extractNumber = string => parseFloat(string.match(/\d+/)[0]);

    const uniqueLinks = uniqBy(req.body.links.reverse().filter(link => link['Domain URL']),
        link => [link.editor, link['Domain URL'], link['Link Type']].join());
    const mappedLinks = uniqueLinks.map(link => ({
        editor: mongoose.Types.ObjectId(link.editor),
        url: link['Domain URL'],
        tat: extractNumber(link['TAT']),
        link: link['Link Type'] && link['Link Type'].trim(),
        anchor: link['Anchor Type'] && link['Anchor Type'].trim(),
        price: extractNumber(link['Price']),
        currency: link.currency,
        notes: link['Notes'],
    }));

    try {
        const results = await Link.collection.insertMany(mappedLinks, {ordered: false});
        res.send(results);
    } catch (e) {
        if(e.code === 11000 /* Duplication error */) {
            let duplicatedLinks;
            if(!e.writeErrors) { // Single duplication
                duplicatedLinks = [e.getOperation()];
            } else { // Multiple duplications
                duplicatedLinks = e.writeErrors.map(err => err.getOperation() );
            }
            const updateRequests = duplicatedLinks.map(link => createUpdateLinkRequest(link));
            const result = await Promise.all(updateRequests);
            res.send(result)
        } else {
            res.send(e);
        }
    }
}

function createUpdateLinkRequest(originalLink) {
    const {editor, url, link} = originalLink;
    delete originalLink._id;
    return Link.update({editor, url, link}, originalLink);
}

function filterEditorsWorkbooks(workbooks) {
    return workbooks.filter((workbook) => {
        workbook.currency = workbook.workbook_name.indexOf('EUR') > -1 ? 'EUR' : 'USD';
        return workbook.workbook_name.indexOf('Editor') > -1;
    });
}

async function deleteLinks(req, res) {
    const { ids } = req.body;
    try {
        await Link.deleteMany({_id: { $in: ids }})
        res.send({ids});
    } catch(e) {
        console.log(e)
        res.send(e);
    }
}

async function updateLink(req, res) {
    const { link } = req.body;
    try {
        await Link.updateOne({_id: link._id}, link);
        res.send(link);
    } catch(e) {
        console.log(e);
        res.send(e);
    }
}

module.exports = {
    generateClientSpreadsheet,
    generateClientLinks,
    getClientLinks,
    addLinks,
    generateInternalSpreadsheet,
    deleteLinks,
    updateLink,
};
