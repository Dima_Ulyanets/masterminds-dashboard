const boom = require('boom');
const axios = require('axios');
const cheerio = require('cheerio');
const { pearsonCorrelation } = require('../helpers/stats');

const scrapeURL = (req, res, next) => {
    const { links, keyword} = req.body;
    scrapeLinks(links, keyword).then(result => {
        res.send(result);
    });
};

async function scrapeLinks(links, keyword) {
    const results = [];
    let errorsNumber = 0;
    for(const link of links) {
        console.log('parsing: ', link)
        try {
            const response = await axios({
                url: link,
                method: 'GET',
                strictSSL: false,
            }).then(response => {
                const pageHtml = response.data;
                const re = new RegExp(keyword, 'gi');
                const $ = cheerio.load(pageHtml);
                const bodyText = $('body').html().replace(/<\/?[^>]+(>|$)/g, "");
                const result = bodyText.match(re);
                const bodyWordsNumber = bodyText.split(' ').length;
                return {
                    url: link,
                    body: {
                        exactKeywordsNumber: result.length,
                        exactKeywordsDensity: result.length / bodyWordsNumber,
                    },

                };
            });
            results.push(response);
        } catch(e) {
            console.log(e);
            errorsNumber ++;
        }
    }

    const data = [
        [...Array(results.length).keys()],
        results.map(result => result.body.exactKeywordsNumber),
    ];

    const dataDensity = [
        [...Array(results.length).keys()],
        results.map(result => result.body.exactKeywordsDensity),
    ];
    const correlation = pearsonCorrelation(data, 0, 1);
    const correlationDensity = pearsonCorrelation(dataDensity, 0, 1);

    console.log('scraping done, errors: ', errorsNumber);

    return {
        serp: results,
        exactKeywordsCorrelation: correlation,
        exactKeywordsDensityCorrelation: correlationDensity,
    };
}

module.exports = {
    scrapeURL,
};
